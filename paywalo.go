package paywalo

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"net/http"
)

type (
	// Paywalo type
	Paywalo struct {
		Config Config
	}
	// Config define of Paywalo lib config
	Config struct {

		// Username define for API credentail
		Username string

		// Password define for API credentail
		Password string

		// URL define for API endpoint
		URL string

		// Debug define debug flog
		Debug bool

		// EndPoints define for each functions
		EndPoints EndPoints
	}
	// EndPoints define endpoints for API's functions
	EndPoints struct {
		GetDepositBankAccount string
		GetDepositOnlineBanks string
		VerifyDepositOrder    string
		ConfirmDepositOnline  string
		CreateOrder           string
	}
	// BankAccountPaywalo response message from API
	BankAccountPaywalo struct {
		BankID        int    `json:"id"`
		BankName      string `json:"bankName"`
		BankCode      string `json:"bankCode"`
		BankURL       string `json:"bankUrl,-"`
		AccountNumber string `json:"accountNumber"`
		AccountName   string `json:"accountName"`
	}
	// OnlineBank type for response
	OnlineBank struct {
		BankCode    string `json:"code"`
		BankName    string `json:"name"`
		Description string `json:"description"`
		Currency    string `json:"currency"`
	}
	oderRequest struct {
		OrderNumber     string  `json:"OrderNumber"`
		MemberID        string  `json:"MemberId,omitempty"`
		Username        string  `json:"Username,omitempty"`
		FullName        string  `json:"Fullname,omitempty"`
		PromotionCode   string  `json:"PromotionCode,omitempty"`
		BankID          int     `json:"BankId,omitempty"`
		ImageSlipBase64 string  `json:"ImageSlipBase64,omitempty"`
		Debug           bool    `json:"test"`
		Amount          float64 `json:"Amount,omitempty"`
		BankCode        string  `json:"BankCode,omitempty"`
		TransactionType int     `json:"Transaction_type,omitempty"`
		TransMethod     int     `json:"Transaction_method,omitempty"`
	}
	// Response return response from verify request
	Response struct {
		Status      string `json:"status"`
		Order       string `json:",omitempty"`
		Desc        string `json:",omitempty"`
		Transaction struct {
			OrderNumber       string  `json:"orderNumber,omitempty"`
			MemberID          string  `json:"memberID,omitempty"`
			Amount            float64 `json:"amount,omitempty"`
			OrderDate         string  `json:"orderDate,omitempty"`
			ModifiedDate      string  `json:"modifiedDate,omitempty"`
			TransactionType   string  `json:"transactionType,omitempty"`
			PaymentMethod     string  `json:"paymentMethod,omitempty"`
			PaymentGateWayURL string  `json:"paymentGateWayUrl,omitempty"`
			QrCodeImageURL    string  `json:"qrCodeImageUrl,omitempty"`
			Currency          string  `json:"currency,omitempty"`
		} `json:",omitempty"`
	}
)

var (
	// DefaultConfig define for config init
	DefaultConfig = Config{
		Username: "admin",
		Password: "Addlink123!",
		URL:      "http://localhost:8042",
		Debug:    false,
		EndPoints: EndPoints{
			GetDepositBankAccount: "/api/common/getBankAccount",
			GetDepositOnlineBanks: "/api/common/getOnlineBank",
			VerifyDepositOrder:    "/api/confirm/deposit/banktransfer",
			ConfirmDepositOnline:  "/api/confirm/deposit/online",
			CreateOrder:           "/api/order/new",
		},
	}
)

// New to create paywalo service
func New() (p *Paywalo) {
	p = &Paywalo{
		Config: DefaultConfig,
	}
	return
}

// GetBankAccount return deposit bankaccounts
func (p *Paywalo) GetBankAccount() (*[]BankAccountPaywalo, error) {
	client := &http.Client{}
	req, err := http.NewRequest("GET", p.Config.URL+p.Config.EndPoints.GetDepositBankAccount, nil)

	if err != nil {
		log.Printf("Error while create HTTP Request to payment API %+v\n", err)
		return nil, err
	}
	req.SetBasicAuth(p.Config.Username, p.Config.Password)

	resp, err := client.Do(req)

	if err != nil {
		log.Printf("Error while send request to payment API: %+v\n", err)
		return nil, err
	}

	bodyText, err := ioutil.ReadAll(resp.Body)
	var baPW []BankAccountPaywalo
	err = json.Unmarshal(bodyText, &baPW)
	if err != nil {
		log.Printf("Error while unmarshal: %+v\n", err)
		return nil, err
	}
	return &baPW, err
}

// GetOnelineBanks return list of bank for online deposit
func (p *Paywalo) GetOnelineBanks() (*[]OnlineBank, error) {
	client := &http.Client{}
	req, err := http.NewRequest("GET", p.Config.URL+p.Config.EndPoints.GetDepositOnlineBanks, nil)

	if err != nil {
		log.Printf("Error while create HTTP Request to payment API %+v\n", err)
		return nil, err
	}
	req.SetBasicAuth(p.Config.Username, p.Config.Password)

	resp, err := client.Do(req)

	if err != nil {
		log.Printf("Error while send request to payment API: %+v\n", err)
		return nil, err
	}

	bodyText, err := ioutil.ReadAll(resp.Body)
	var onlineBanks []OnlineBank
	err = json.Unmarshal(bodyText, &onlineBanks)
	if err != nil {
		log.Printf("Error while unmarshal: %+v\n", err)
		return nil, err
	}
	return &onlineBanks, err
}

// VerifyOrder verify deposit order with slip image
func (p *Paywalo) VerifyOrder(orderID string, username string, bankID int, imgBase64 string) (*Response, error) {
	postData := oderRequest{
		OrderNumber:     orderID,
		MemberID:        username,
		BankID:          bankID,
		ImageSlipBase64: imgBase64,
		Debug:           p.Config.Debug,
	}
	b := new(bytes.Buffer)
	err := json.NewEncoder(b).Encode(postData)
	if p.Config.Debug {
		log.Printf("DEBUG - Post Body: %s\n", b)
	}
	if err != nil {
		log.Printf("Error while binding request body to payment API %+v\n", err)
		return nil, err
	}
	client := &http.Client{}
	req, err := http.NewRequest("POST", p.Config.URL+p.Config.EndPoints.VerifyDepositOrder, b)

	if err != nil {
		log.Printf("Error while create HTTP Request to payment API %+v\n", err)
		return nil, err
	}
	req.SetBasicAuth(p.Config.Username, p.Config.Password)
	req.Header.Add("Content-Type", "application/json; charset=utf-8")
	resp, err := client.Do(req)
	if err != nil {
		log.Printf("Error while create HTTP Request to payment API %+v\n", err)
		return nil, err
	}
	bodyText, err := ioutil.ReadAll(resp.Body)
	if p.Config.Debug {
		log.Printf("DEBUG - Response Code: %s\n", resp.Status)
		log.Printf("DEBUG - Response Body: %s\n", bodyText)
	}
	if resp.StatusCode != 200 {
		return nil, errors.New("not successfuly request to payment api")
	}
	var apiResponse Response
	err = json.Unmarshal(bodyText, &apiResponse)
	if err != nil {
		log.Printf("Error while unmarshal: %+v\n", err)
		return nil, err
	}

	return &apiResponse, nil
}

// CreateOnlineDeposit to create a deposit online order
func (p *Paywalo) CreateOnlineDeposit(orderID string, username string, fullname string, amount float64, bankCode string, promoCode string) (*Response, error) {
	postData := oderRequest{
		OrderNumber:     orderID,
		Username:        username,
		Amount:          amount,
		BankCode:        bankCode,
		PromotionCode:   promoCode,
		TransactionType: 1,
		TransMethod:     2,
		Debug:           p.Config.Debug,
	}
	b := new(bytes.Buffer)
	err := json.NewEncoder(b).Encode(postData)
	if err != nil {
		log.Printf("Error while binding request body to payment API %+v\n", err)
		return nil, err
	}

	client := &http.Client{}
	req, err := http.NewRequest("POST", p.Config.URL+p.Config.EndPoints.CreateOrder, b)
	// loging for debug
	if p.Config.Debug {
		log.Printf("DEBUG - Post Body: %s\n", b)
	}
	if err != nil {
		log.Printf("Error while create HTTP Request to payment API %+v\n", err)
		return nil, err
	}
	req.SetBasicAuth(p.Config.Username, p.Config.Password)
	req.Header.Add("Content-Type", "application/json; charset=utf-8")
	resp, err := client.Do(req)
	if err != nil {
		log.Printf("Error while Request to payment API %+v\n", err)
		return nil, err
	}
	if resp.StatusCode != 200 {
		return nil, errors.New("not successfuly request to payment api")
	}
	bodyText, err := ioutil.ReadAll(resp.Body)
	var apiResponse Response
	// loging for debug
	if p.Config.Debug {
		log.Printf("DEBUG - Response Code: %s\n", resp.Status)
		log.Printf("DEBUG - Response Body: %s\n", bodyText)
	}
	err = json.Unmarshal(bodyText, &apiResponse)
	if err != nil {
		log.Printf("Error while unmarshal: %+v\n", err)
		return nil, err
	}

	postDataConfirm := oderRequest{
		OrderNumber: orderID,
		MemberID:    username,
		Amount:      amount,
		BankCode:    bankCode,
		FullName:    fullname,
		Debug:       p.Config.Debug,
	}

	err = json.NewEncoder(b).Encode(postDataConfirm)
	if err != nil {
		log.Printf("Error while binding confirm request body to payment API %+v\n", err)
		return nil, err
	}

	req, err = http.NewRequest("POST", p.Config.URL+p.Config.EndPoints.ConfirmDepositOnline, b)
	// loging for debug
	if p.Config.Debug {
		log.Printf("DEBUG - Post Body confirm: %s\n", b)
	}
	if err != nil {
		log.Printf("Error while create HTTP Request confirm to payment API %+v\n", err)
		return nil, err
	}
	req.SetBasicAuth(p.Config.Username, p.Config.Password)
	req.Header.Add("Content-Type", "application/json; charset=utf-8")
	resp, err = client.Do(req)
	if err != nil {
		log.Printf("Error while Request confirm to payment API %+v\n", err)
		return nil, err
	}

	bodyText, err = ioutil.ReadAll(resp.Body)
	// loging for debug
	if p.Config.Debug {
		log.Printf("DEBUG - Confirm Response Code: %s\n", resp.Status)
		log.Printf("DEBUG - Confirm Response Body: %s\n", bodyText)
	}
	if resp.StatusCode != 200 {
		return nil, errors.New("not successfuly request confirm to payment api")
	}
	err = json.Unmarshal(bodyText, &apiResponse)
	if err != nil {
		log.Printf("Error while unmarshal confirm: %+v\n", err)
		return nil, err
	}
	return &apiResponse, nil
}

// CreateOfflineDeposit to create a deposit offline order
func (p *Paywalo) CreateOfflineDeposit(orderID string, username string, bankID int, amount float64, promoCode string) (*Response, error) {
	postData := oderRequest{
		OrderNumber:     orderID,
		Username:        username,
		Amount:          amount,
		PromotionCode:   promoCode,
		BankID:          bankID,
		TransactionType: 1,
		TransMethod:     1,
		Debug:           p.Config.Debug,
	}
	b := new(bytes.Buffer)
	err := json.NewEncoder(b).Encode(postData)
	if err != nil {
		log.Printf("Error while binding request body to payment API %+v\n", err)
		return nil, err
	}
	if p.Config.Debug {
		log.Printf("DEBUG - Post Body: %s\n", b)
	}
	client := &http.Client{}
	req, err := http.NewRequest("POST", p.Config.URL+p.Config.EndPoints.CreateOrder, b)

	if err != nil {
		log.Printf("Error while create HTTP Request to payment API %+v\n", err)
		return nil, err
	}
	req.SetBasicAuth(p.Config.Username, p.Config.Password)
	req.Header.Add("Content-Type", "application/json; charset=utf-8")
	resp, err := client.Do(req)
	if err != nil {
		log.Printf("Error while create HTTP Request to payment API %+v\n", err)
		return nil, err
	}
	bodyText, err := ioutil.ReadAll(resp.Body)
	var apiResponse Response
	if p.Config.Debug {
		log.Printf("DEBUG - Response Code: %s\n", resp.Status)
		log.Printf("DEBUG - Response Body: %s\n", bodyText)
	}
	if resp.StatusCode != 200 {
		return nil, errors.New("not successfuly request to payment api")
	}

	err = json.Unmarshal(bodyText, &apiResponse)
	if err != nil {
		log.Printf("Error while unmarshal: %+v\n", err)
		return nil, err
	}
	return &apiResponse, nil
}
